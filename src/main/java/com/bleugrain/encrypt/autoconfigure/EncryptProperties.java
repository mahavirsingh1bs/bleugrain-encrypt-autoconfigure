package com.bleugrain.encrypt.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "bleugrain.encrypt")
public class EncryptProperties {
	private String key = "JamesBarkAtMeAt$";
	private String initVector = "Hack18YearsOld27";
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getInitVector() {
		return initVector;
	}
	public void setInitVector(String initVector) {
		this.initVector = initVector;
	}
	
}
