package com.bleugrain.encrypt.autoconfigure;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.carefarm.prakrati.security.Encryptor;
import com.carefarm.prakrati.security.impl.DefaultEncryptorImpl;

import lombok.extern.slf4j.Slf4j;

@Configuration
@ConditionalOnClass(Encryptor.class)
@EnableConfigurationProperties(EncryptProperties.class)
@Slf4j
public class BleugrainEncryptAutoConfiguration {
	
	@Autowired
	private EncryptProperties properties;
	
	@Bean
	@ConditionalOnMissingBean
	public Encryptor encryptor(Properties mailProperties) {
		log.info("defining encryptor bean");
		return new DefaultEncryptorImpl(properties.getKey(), properties.getInitVector());
	}
	
}
